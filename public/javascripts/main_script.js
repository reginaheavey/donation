$('.ui.dropdown').dropdown();

$('.ui.form').form({
	// validation of candidate dd on donation page
	candidate : {
		identifier : 'candidateEmail',
		rules : [ {
			type : 'empty',
			prompt : 'Please select a candidate'
		} ]
	},

	// validation of amount donated dd on donation page
	amountDonated : {
		identifier : 'amountDonated',
		rules : [ {
			type : 'empty',
			prompt : 'Please select an amount to donate'
		} ]
	},
	
	// validation of on signup page
	firstName : {
		identifier : 'user.firstName',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter First Name'
		} ]
	},

	lastName : {
		identifier : 'user.lastName',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter Last Name'
		} ]
	},

	age : {
		identifier : 'user.age',
		rules : [ {
			type : 'integer',
			prompt : 'Age must be a number'
		} ]
	},

	address1 : {
		identifier : 'user.address1',
		rules : [ {
			type : 'empty',
			prompt : 'Address 1 details must be entered'
		} ]
	},

	city : {
		identifier : 'user.city',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter City'
		} ]
	},

	state : {
		identifier : 'user.state',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter State'
		} ]
	},

	zipCode : {
		identifier : 'user.zipCode',
		rules : [ {
			type : 'integer',
			prompt : 'Zip Code must be a number'
		}, {
			type : 'length[5]',
			prompt : 'Zip Code must be 5 digits'
		} ]
	},

	email : {
		identifier : 'user.email',
		rules : [ {
			type : 'email',
			prompt : 'Please enter valid Email'
		} ]
	},

	password : {
		identifier : 'user.password',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter a password'
		}, {
			type : 'length[6]',
			prompt : 'Your password must be at least 6 characters'
		} ]
	},

	// validation for administrator login
	adminUsername : {
		identifier : 'admin.adminUsername',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter a username'
		} ]
	},

	adminPassword : {
		identifier : 'admin.adminPassword',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter a password'
		}, {
			type : 'length[6]',
			prompt : 'Your password must be at least 6 characters'
		} ]
	},

	//validation for register candidate
	candidateFirstName : {
		identifier : 'candidate.candidateFirstName',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter candidate first name'
		} ]
	},

	candidateLastName : {
		identifier : 'candidate.candidateLastName',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter candidate last name'
		} ]
	},

	candidateEmail : {
		identifier : 'candidate.candidateEmail',
		rules : [ {
			type : 'email',
			prompt : 'Please enter a valid candidate email'
		} ]
	},

	candidatePassword : {
		identifier : 'candidate.candidatePassword',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter a password'
		}, {
			type : 'length[6]',
			prompt : 'Your password must be at least 6 characters'
		} ]
	},

	candidateOffice : {
		identifier : 'candidate.office',
		rules : [ {
			type : 'empty',
			prompt : 'Please select candidate office'
		} ]
	},

	office : {
		identifier : 'office.office',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter office title'
		} ]
	},

	officeDesc : {
		identifier : 'office.officeDesc',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter office description'
		} ]
	},

	targetAmount : {
		identifier : 'candidate.targetAmount',
		rules : [ {
			type : 'integer',
			prompt : 'Target Amount must be a number'
		} ]
	},

	latitude : {
		identifier : 'geolocation.latitude',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter latitude in range +85 to -85'
		}, {
			type : 'isValidLatitude',
			prompt : 'Please enter latitude: valid range -85.0 to +85.0'
		} ]
	},

	longitude : {
		identifier : 'geolocation.longitude',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter longitude in range +180 to -180'
		}, {
			type : 'isValidLongitude',
			prompt : 'Please enter longitude: valid range -180.0 to +180.0'
		} ]
	},

	editLatitude : {
		identifier : 'latitude',
		rules : [ {
			type : 'isValidLatitude',
			prompt : 'Please enter latitude: valid range -85.0 to +85.0'
		} ]
	},

	editLongitude : {
		identifier : 'longitude',
		rules : [ {
			type : 'isValidLongitude',
			prompt : 'Please enter longitude: valid range -180.0 to +180.0'
		} ]
	},

});
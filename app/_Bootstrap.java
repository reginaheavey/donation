import java.util.List;

import play.*;
import play.jobs.*;
import play.test.*;
 
import models.*;
 
@OnApplicationStart
public class _Bootstrap extends Job 
{ 
  public void doJob()
  {
    Fixtures.delete();
    Fixtures.loadModels("data.yml");
  }
}
package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

import java.util.Date;
 
@Entity
public class Donation extends Model
{  
  public long received;
  public String methodDonated;
  public Date dateDonated;
 
  @ManyToOne
  public User from;
 
  @ManyToOne
  public Candidate candidate;

  public Donation(User from, long received, String methodDonated, Date dateDonated, Candidate candidate)
  {
    this.received = received;
    this.methodDonated = methodDonated;
    this.from = from;
    this.dateDonated = new Date();
    this.candidate = candidate;
  }

  public static List<Donation> findByEmail(String string)
  {
    return null;
  }
}
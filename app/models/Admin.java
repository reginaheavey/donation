package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

import java.util.Date;

@Entity
public class Admin extends Model
{ 
  public String adminUsername;
  public String adminFirstName;
  public String adminLastName;
  public String adminPassword;
 
  public boolean checkPassword(String password)
  {
    return this.adminPassword.equals(password);
  } 

  public static Admin findByUserName(String adminUsername2)
  {
    return find("adminUsername", adminUsername2).first();
  }
} 

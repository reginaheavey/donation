package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

import java.util.Date;

@Entity  
public class Candidate extends Model
{
  public String candidateFirstName;
  public String candidateLastName;
  public String candidateEmail;
  public String candidatePassword;
  public String office;
  public long targetAmount;
 
  @OneToMany(mappedBy = "candidate")
  List<Donation> donations;

  public static Candidate findByEmail(String candidateEmail)
  {
    Logger.info("Candidate model - searching candidate by email: " + candidateEmail);
    return find("candidateEmail", candidateEmail).first();
  }

  public Candidate(String candidateFirstName, String candidateLastName)
  {
    this.candidateFirstName = candidateFirstName;
    this.candidateLastName = candidateLastName;
  }
}

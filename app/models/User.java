package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class User extends Model
{   
  @OneToMany(mappedBy = "from", cascade = CascadeType.ALL)
  List<Donation> donations = new ArrayList<Donation>();
 
  // relationship between geolocation and user - a geolocation can belong to
  // many users
  @ManyToOne
  public Geolocation location;

  // @ManyToOne
  // public Candidate candidate;

  public boolean usCitizen;
  public String firstName;
  public String lastName;
  public int age;
  public String address1;
  public String address2;
  public String city;
  public String state;
  public int zipCode;
  public String email;
  public String password;

  public static User findByEmail(String email)
  {
    return find("email", email).first();
  }

  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }

  public User(Geolocation location)
  {
    this.location = location;
  }

  @Override
  public String toString()
  {
    return "User [donations=" + donations + ", usCitizen=" + usCitizen + ", firstName=" + firstName
        + ", lastName=" + lastName + ", age=" + age + ", address1=" + address1 + ", address2=" + address2
        + ", city=" + city + ", state=" + state + ", zipCode=" + zipCode + ", email=" + email + ", password="
        + password + "]";
  }

}
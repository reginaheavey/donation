package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

import java.util.Date;

@Entity
public class Geolocation extends Model
{
  public String latitude;
  public String longitude;

  // one geolocation can belong to many users
  @OneToMany(mappedBy = "location")
  List<User> users = new ArrayList<User>();
 
}
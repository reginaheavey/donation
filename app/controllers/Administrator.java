package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Administrator extends Controller
{
  public static void index()
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        Logger.info("Landed in Administrator Controller");
        render();
      }
  }

  public static void adminReport()
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        Logger.info("Landed in Administrator adminReport");
        List<Donation> donations = Donation.findAll();
        List<Candidate> candidates = Candidate.findAll();
        render(donations, candidates);
      }
  }
}

package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Report extends Controller
{

  public static void filterByCandidate(String candidateEmail)
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        List<Candidate> candidates = Candidate.findAll();
        List<Donation> uniqueDonations = getDonors();
        List<Donation> states = getStates();
        ArrayList<Donation> donations = new ArrayList<Donation>();
        List<Donation> allDonations = Donation.findAll();
        for (Donation donation : allDonations)
          if (donation.candidate.candidateEmail.equalsIgnoreCase(candidateEmail))
            {
              donations.add(donation);
            }
        render("Report/renderReport.html", donations, candidates, uniqueDonations, states);
      }
  }

  public static void filterByDonor(String email)
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        List<Candidate> candidates = Candidate.findAll();
        List<Donation> uniqueDonations = getDonors();
        List<Donation> states = getStates();
        ArrayList<Donation> donations = new ArrayList<Donation>();
        List<Donation> allDonations = Donation.findAll();
        for (Donation donation : allDonations)
          if (donation.from.email.equalsIgnoreCase(email))
            {
              donations.add(donation);
            }
        render("Report/renderReport.html", donations, candidates, uniqueDonations, states);
      }
  }

  public static List<Donation> getDonors()
  {
    List<Donation> listDonations = new ArrayList<Donation>();
    Set<String> emailSet = new HashSet<String>();
    List<Donation> donations = Donation.findAll();
    for (Donation donation : donations)
      if (emailSet.add(donation.from.email))
        {
          listDonations.add(donation);
        }
    return listDonations;
  }

  public static void filterByState(String state)
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        List<Candidate> candidates = Candidate.findAll();
        List<Donation> uniqueDonations = getDonors();
        List<Donation> states = getStates();
        ArrayList<Donation> donations = new ArrayList<Donation>();
        List<Donation> allDonations = Donation.findAll();
        for (Donation donation : allDonations)
          if (donation.from.state.equalsIgnoreCase(state))
            {
              donations.add(donation);
            }
        render("Report/renderReport.html", donations, candidates, uniqueDonations, states);
      }
  }

  private static List<Donation> getStates()
  {
    List<Donation> listStates = new ArrayList<Donation>();
    Set<String> statesSet = new HashSet<String>();
    List<Donation> donations = Donation.findAll();
    for (Donation donation : donations)
      if (statesSet.add(donation.from.state))
        {
          listStates.add(donation);
        }
    return listStates;
  }

  public static void renderReport()
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        Logger.info("Landed in renderReport");
        // first time in all donations, candidates & donors in dd are listed
        List<Donation> donations = Donation.findAll();
        List<Candidate> candidates = Candidate.findAll();
        List<Donation> uniqueDonations = getDonors();
        List<Donation> states = getStates();
        render(donations, candidates, uniqueDonations, states);
      }
  }
  //
  // @Override
  // public String toString()
  // {
  // return "Candidate [candidateFirstName=" + candidateFirstName +
  // ", candidateLastName=" + candidateLastName + ", "
  // + "candidateEmail=" + candidateEmail + ", office=" + office "]";
  // }

}
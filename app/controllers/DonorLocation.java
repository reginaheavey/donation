package controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import models.User;
import play.Logger;
import play.mvc.Controller;

public class DonorLocation extends Controller
{
  public static void index()
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        render();
      }
  }

  /**
   * Create list sample positional data Necessary to add json-simple-1.1.jar or
   * equivalent to lib folder Then add jar to build path and import here
   */
  public static void listGeolocations()
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        Logger.info("Landed in Donor Location controller listGeolocations method");
        List<List<String>> jsonArray = new ArrayList<List<String>>();
        List<User> users = User.findAll();
        int count = 0;
        for (User user : users)
          {
            jsonArray.add(count,
                Arrays.asList(user.firstName, user.location.latitude, user.location.longitude));
            count += 1;

          }
        renderJSON(jsonArray);
      }
  }
}
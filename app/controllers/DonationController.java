package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;
import play.mvc.Controller;

public class DonationController extends Controller
{
  public static void index(String candidateEmail)
  {
    User user = Accounts.getCurrentUser();
    if (user == null)
      {
        Logger.info("Donation class : Unable to getCurrentuser");
        Accounts.login();
      }
    else
      {
        List<Candidate> candidates = Candidate.findAll();
        if (candidateEmail == null)
          {
            String donationProgress = " ";
            String percentProgress = " ";
            render(candidates, user, donationProgress, percentProgress);
          }
        else
          {
            String prog = getPercentTargetAchieved(candidateEmail);
            Candidate found = Candidate.findByEmail(candidateEmail);
            String percentProgress = prog + "%";
            String donationProgress = prog + "%" + " target achieved to date for candidate "
                + found.candidateFirstName + " " + found.candidateLastName;
            render(candidates, user, donationProgress, percentProgress);
          }
      }
  }

  public static void donate(long amountDonated, String methodDonated, Date dateDonated, String candidateEmail)
  {
    User user = Accounts.getCurrentUser();
    if (user == null)
      {
        Logger.info("Donation class : Unable to getCurrentuser");
        Accounts.login();
      }
    else
      {
        addDonation(user, amountDonated, methodDonated, dateDonated, candidateEmail);
      }
    index(candidateEmail);
  }

  private static void addDonation(User user, long amountDonated, String methodDonated, Date dateDonated,
      String candidateEmail)
  {
    Candidate found = Candidate.findByEmail(candidateEmail);
    Donation bal = new Donation(user, amountDonated, methodDonated, dateDonated, found);
    bal.save();
  }

  private static long getDonationTarget(String candidateEmail)
  {
    Candidate found = Candidate.findByEmail(candidateEmail);
    return found.targetAmount;

  }

  public static String getPercentTargetAchieved(String candidateEmail)
  {
    List<Donation> allDonations = Donation.findAll();
    long total = 0;
    for (Donation donation : allDonations)
      {
        if (donation.candidate.candidateEmail.equalsIgnoreCase(candidateEmail))
          {
            total += donation.received;
          }
      }
    long target = getDonationTarget(candidateEmail);
    long percentAchieved = (total * 100 / target);
    percentAchieved = percentAchieved > 100 ? 100 : percentAchieved;
    String percentProgress = String.valueOf(percentAchieved);
    return percentProgress;
  }

  public static void listGeolocations()
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        List<List<String>> jsonArray = new ArrayList<List<String>>();
        List<User> users = User.findAll();
        int count = 0;
        for (User user : users)
          {
            jsonArray.add(count,
                Arrays.asList(user.firstName, user.location.latitude, user.location.longitude));
            count += 1;

          }
        // Logger.info("jsonArray" + jsonArray);
        renderJSON(jsonArray);
      }
  }
}
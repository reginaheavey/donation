package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class CandidateController extends Controller
{

  public static void regCandidate()
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        Logger.info("Landed in Candidate Controller regCandidate method");
        List<Office> offices = Office.findAll();
        render(offices);
      }
  }

  public static void registerCandidate(Candidate candidate)
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        Logger.info("Landed in Candidate Controller registerCandidate method");
        Logger.info("Check 1: Checking if candidate is already registered " + candidate.candidateEmail);
        Candidate existingCandidate = Candidate.findByEmail(candidate.candidateEmail);
        if (existingCandidate != null)
          {
            Logger.info("Candidate already exists " + existingCandidate.candidateFirstName + " "
                + existingCandidate.candidateLastName + " with email: " + existingCandidate.candidateEmail);
            String error = "regCandidate";
            render("Accounts/error.html", error);
          }
        else
          {
            Logger.info("Register info: " + candidate.candidateFirstName + " " + candidate.candidateLastName
                + " " + candidate.candidateEmail + " " + candidate.office + " " + candidate.targetAmount);
            candidate.save();
            List<Donation> donations = Donation.findAll();
            List<Candidate> candidates = Candidate.findAll();

            render("Administrator/adminReport.html", donations, candidates);
          }
      }
  }
}

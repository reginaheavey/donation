package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Accounts extends Controller
{
  public static void signup()
  {
    render();
  }

  public static void error(String error)
  {
    render(error);
  }

  public static void register(User user, Geolocation geolocation)
  {
    Logger.info("Check 1: Checking if user is already registered " + user.email);
    User existingUser = User.findByEmail(user.email);
    if (existingUser != null)
      {
        Logger.info("User already exists " + existingUser.firstName + " " + existingUser.lastName
            + " with email: " + existingUser.email);
        error("register");
      }
    // Server side age validation
    Logger.info("Check 2: Checking if user age is valid " + user.age);
    if (user.age < 1 || user.age > 150)
      {
        Logger.info("User age is invalid");
        error("age");
      }
    // Server side zipCode validation
    Logger.info("Check 3: Checking if zip code is valid " + user.zipCode);
    if (user.zipCode <= 0 || user.zipCode > 99999)
      {
        Logger.info("zipCode is invalid");
        error("zipCode");
      }
    else
      {
        geolocation.save();
        User toSave = new User(geolocation);
        user.location = toSave.location;
        user.save();
        login();
      }
  }

  public static void login()
  {
    render();
  }

  public static void logout()
  {
    session.clear();
  }

  public static void authenticate(User user)
  {
    Logger.info("Attempting to authenticate with " + user.toString());
    User existingUser = User.findByEmail(user.email);
    if ((existingUser != null) && (existingUser.checkPassword(user.password) == true))
      {
        Logger.info("Successful authentication of " + existingUser.firstName + " " + existingUser.lastName);
        session.put("logged_in_userid", existingUser.id);
        String candidateEmail = null;
        DonationController.index(candidateEmail);
      }
    else
      {
        Logger.info("Authentication failed" + user.email + " " + user.password);
        error("authenticate");
      }
  }

  public static void authenticateAdmin(Admin admin)
  {
    Admin existingAdmin = Admin.findByUserName(admin.adminUsername);
    if ((existingAdmin != null) && (existingAdmin.checkPassword(admin.adminPassword) == true))
      {
        Administrator.adminReport();
      }
    else
      {
        Logger.info("Authentication failed" + admin.adminUsername + " " + admin.adminPassword);
        error("adminAuthenticate");
      }
  }

  public static User getCurrentUser()
  {
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        return null;
      }
    User logged_in_user = User.findById(Long.parseLong(userId));
    Logger.info("In Accounts controller: Logged in user is " + logged_in_user.firstName);
    return logged_in_user;
  }

  public static void edit()
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        login();
      }
    User user = User.findById(Long.parseLong(userId));

    String latitude = getLatitude(user);
    String longitude = getLongitude(user);
    render(user, latitude, longitude);
  }

  private static String getLatitude(User user)
  {
    String latitude = null;
    List<Geolocation> allGeolocation = Geolocation.findAll();
    for (Geolocation geolocation : allGeolocation)
      {
        if (geolocation == user.location)
          {
            latitude = geolocation.latitude;
          }
      }
    return (latitude);
  }

  private static String getLongitude(User user)
  {
    String longitude = null;
    List<Geolocation> allGeolocation = Geolocation.findAll();
    for (Geolocation geolocation : allGeolocation)
      {
        if (geolocation == user.location)
          {
            longitude = geolocation.longitude;
          }
      }
    return (longitude);
  }

  public static void update(String firstName, String lastName, int age, String address1, String address2,
      String city, String state, int zipCode, String latitude, String longitude)
  {
    String userId = session.get("logged_in_userid");
    User user = User.findById(Long.parseLong(userId));

    if (!firstName.isEmpty())
      {
        user.firstName = firstName;
      }
    if (!lastName.isEmpty())
      {
        user.lastName = lastName;
      }
    if (age != 0)
      {
        user.age = age;
      }
    if (!address1.isEmpty())
      {
        user.address1 = address1;
      }
    if (!address2.isEmpty())
      {
        user.address2 = address2;
      }
    if (!city.isEmpty())
      {
        user.city = city;
      }
    if (!state.isEmpty())
      {
        user.state = state;
      }
    if (zipCode != 0)
      {
        user.zipCode = zipCode;
      }

    if (!latitude.isEmpty())
      {
Logger.info("editLatitude " + latitude);
        List<Geolocation> allGeolocation = Geolocation.findAll();
        for (Geolocation geolocation : allGeolocation)
          {
            if (geolocation.id == user.id)
              {
                geolocation.latitude = latitude;
                geolocation.save();
              }
          }
      }

    if (!longitude.isEmpty())
      {
        List<Geolocation> allGeolocation = Geolocation.findAll();
        for (Geolocation geolocation : allGeolocation)
          {
            if (geolocation.id == user.id)
              {
                geolocation.longitude = longitude;
                geolocation.save();
              }
          }
      }
    user.save();

    String candidateEmail = null;
    DonationController.index(candidateEmail);
  }
}

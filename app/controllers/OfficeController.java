package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class OfficeController extends Controller
{
  public static void regOffice()
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        Logger.info("Landed in Office Controller regOffice method");
        render();
      }
  }

  public static void registerOffice(Office office)
  {
    // check so that if user isn't logged in and they try to go to this page
    // they won't get an error, will be directed to login page
    String userId = session.get("logged_in_userid");
    if (userId == null)
      {
        Accounts.login();
      }
    else
      {
        Logger.info("Landed in Office Controller registerOffice method");
        Logger.info("Office info: " + office.office + " " + office.officeDesc);
        office.save();
        List<Donation> donations = Donation.findAll();
        List<Candidate> candidates = Candidate.findAll();

        render("Administrator/adminReport.html", donations, candidates);
      }
  }
}
